<?php
	session_start();
	if (isset($_SESSION["usuario"])){
		
	}else{
		header("Location: index.php");
	}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<meta http-equiv="content-type"
	content="application/xhtml+xml; charset=UTF-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1.0" />
<title>Starter Template - Materialize</title>

<!-- CSS  -->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="css/materialize.css" type="text/css" rel="stylesheet"
	media="screen,projection" />
<link href="css/style.css" type="text/css" rel="stylesheet"
	media="screen,projection" />
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
</head>
<body>
	<nav class="grey lighten-1 center" role="navigation">
		<div class="nav-wrapper container" id="todo">
			<a id="logo-container" href="#" class="brand-logo">NtpQuest</a>
			<ul class="right hide-on-med-and-down">
				<li><a>Cerrar Sesión</a></li>
			</ul>
		</div>
	</nav>


	<!-- Page Layout here -->
	<div class="row">
		<div class="col s3">

			<ul class="collapsible" data-collapsible="accordion">
				<!-- Empieza Nivel 1-->
				<li>
					<div class="collapsible-header grey lighten-1">
						<i class="material-icons">filter_drama</i>Proceso Principal
					</div>
					<div class="collapsible-body">
						<div class="row">
							<div class="col s12 m12">
								<ul class="collapsible" data-collapsible="accordion">
									<li>
										<div class="collapsible-header grey lighten-2">
											<i class="material-icons">filter_drama</i>Aquisición
										</div>
										<div class="collapsible-body">
											<div class="row">
												<div class="col s12 m12">
													<ul class="collapsible" data-collapsible="accordion">
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Inicio
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="a">Requerimientos del sistema</button></form>
																	<form method="POST"><button type="input" name="a2">Empleo previsto del sistema</button></form>
																	<form method="POST"><button type="input" name="a3">Tipo de contrato a emplear</button>
																		</form>
																	<form method="POST"><button type="input" name="a4">Responsabilidades de las organizaciones implicadas</button>
																		</form>
																	<form method="POST"><button type="input" name="a5">Tipos de soporte a usar</button>
																		</form>
																	<form method="POST"><button type="input" name="a6">Consideración de riesgos y su proceso de gestión</button>
																		</form>
																</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Preparación de la solicitud de propuestas
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="b1">Documento de Requerimientos de adquisición</button>
																	<form method="POST"><button type="input" name="b2">Adaptación de procesos, actividades y tareas apropiados de la NTP</button>
																	<form method="POST"><button type="input" name="b3">Definición del alcance de las tareas del contrato</button>
																</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Preparación y actualización del contrato
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="c1">Criterios de evaluación de propuestas</button>
																	<form method="POST"><button type="input" name="c2">Evaluación de proveedores</button>
																	<form method="POST"><button type="input" name="c3">Preparación de contrato con el proveedor</button>
																	<form method="POST"><button type="input" name="c4">Control de modificaciones de contrato</button>
																</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Seguimiento del proveedor
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="d1">Supervisión de las actividades del proveedor</button>
																</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Aceptación y finalización
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="e1">Preparación de la aceptación</button>
																	<form method="POST"><button type="input" name="e2">Documento de Plan de Pruebas</button>
																	<form method="POST"><button type="input" name="e3">Revisiones de aceptación y pruebas de aceptación</button>
																	<form method="POST"><button type="input" name="e4">Gestión de la configuración del producto software entregado</button>
																</div>
															</div>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</li>
									<!-- Empieza Nivel 2-->
									<li>
										<div class="collapsible-header grey lighten-2">
											<i class="material-icons">filter_drama</i>
											Suministro
										</div>
										<div class="collapsible-body">
											<div class="row">
												<div class="col s12 m12">
													<ul class="collapsible" data-collapsible="accordion">
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Inicio
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="f1">Solicitud de Propuesta</button>
																</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Preparación de la respuesta
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="f2">Documento de Oferta</button>
																</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Contrato
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="f3">Documento de Contrato</button>
																</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Planificación
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="g1">Documento de requiitos de adquisición</button>
																	<form method="POST"><button type="input" name="g2">Revisión de Requisitos</button>
																	<form method="POST"><button type="input" name="g3">Documento de ciclo de vida del proyecto</button>
																	<form method="POST"><button type="input" name="g4">Establecer requisitos para el marco de la gestión y aseguramiento del proyecto</button>
																	<form method="POST"><button type="input" name="g5">Documento del plan de gestión y aseguramiento del proyecto</button>
																	<form method="POST"><button type="input" name="g6">Establecimiento de la estructura organizativa del proyecto </button>
																	<form method="POST"><button type="input" name="g7">Establecer entorno de ingenieria</button>
																	<form method="POST"><button type="input" name="g8">Descomposición estructurada del trabajo de los procesos y actividades del ciclo de vida</button>
																	<form method="POST"><button type="input" name="g9">Gestión de las características de calidad de los productos o sevicios software</button>
																	<form method="POST"><button type="input" name="g10">Gestión de la seguridad física y acceso</button>
																	<form method="POST"><button type="input" name="g11">Gestión de subcontratistas</button>
																	<form method="POST"><button type="input" name="g12">Verificación y validación de los procesos</button>
																	<form method="POST"><button type="input" name="g13">Involucración del Adquiriente</button>
																	<form method="POST"><button type="input" name="g14">Involucración del usuario</button>
																	<form method="POST"><button type="input" name="g15">Gestión de riesgo</button>
																	<form method="POST"><button type="input" name="g16">Politicas de seguridad de acceso</button>
																	<form method="POST"><button type="input" name="g17">Aprobación requerida por regulaciones</button>
																	<form method="POST"><button type="input" name="g18">Mecanismos para definir plazos</button>
																	<form method="POST"><button type="input" name="g19">Formación del personal</button>
																</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Ejecución y Control
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="h1">Supervisión y control del proceso y la calidad de los productos o servicios del software</button>
																	<form method="POST"><button type="input" name="h2">Relaciòn con el agente de verificación y validación</button>
																</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Revisión y Evaluación
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="i1">Coordinación de actividades de revisiones del contrato, interfacez y comunicación </button>
																	<form method="POST"><button type="input" name="i2">Soporte de reuniones en general </button>
																	<form method="POST"><button type="input" name="i3">Verificación y validación de satisfacción de requisitos </button>
																	<form method="POST"><button type="input" name="i4">Poner a disposición informes: evaluación, revisiones, etc </button>
																	<form method="POST"><button type="input" name="i5">Informes </button>
																	<form method="POST"><button type="input" name="i6">Actividades de aseguramiento de la calidad </button>
																</div>
															</div>
														</li>


														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Entrega y Finalización
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="i7">Entrega del producto y servicio</button>
																	<form method="POST"><button type="input" name="i8">Asistencia para el soporte y mantenimineto del software</button>
																</div>
															</div>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</li>
									<!-- Termina Nivel 2-->
									<!-- Empieza Nivel 2-->
									<li>
										<div class="collapsible-header grey lighten-2">
											<i class="material-icons">filter_drama</i>Desarrollo
										</div>
										<div class="collapsible-body">
											<div class="row">
												<div class="col s12 m12">
													<ul class="collapsible" data-collapsible="accordion">
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Implementación del proceso
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="j1">Definir modelo de ciclo de vida</button>
																	<form method="POST"><button type="input" name="j2">Documento de salidas</button>
																	<form method="POST"><button type="input" name="j3">Documentación de los problemas y no conformidades encontradas en el software</button>
																	<form method="POST"><button type="input" name="j4">Documento del plan de normas específicas</button>
																	<form method="POST"><button type="input" name="j5">Documento del plan de métodos y herramientas</button>
																	<form method="POST"><button type="input" name="j6">Documento del plan de acciones y responsabilidades</button>
																</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Análisis de los requerimientos del sistema
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="k1">Documento de requerimientos del sistema</button>
																	<form method="POST"><button type="input" name="k2">Documento de especificación de requerimientos del sistema</button>
																	<form method="POST"><button type="input" name="k3">Criterios de evaluación de requerimientos</button>
																	<form method="POST"><button type="input" name="k4">Documento de resultados de las evaluaciones</button>
																</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Diseño de la arquitectura del sistema
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="l1">Establecer arquitectura del sistema a alto nivel</button>
																	<form method="POST"><button type="input" name="l2">Documento de arquitectura del sistema y requerimientos asignados a cada elemento</button>
																	<form method="POST"><button type="input" name="l3">Criterios de evaluación de la arquitectura y requerimientos para los elementos</button>
																	<form method="POST"><button type="input" name="l4">Documento de resultados de las evaluaciones</button>
																</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Análisis de los requerimientos software
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="m1">Especificaciones funcionales y de capacidad</button>
																	<form method="POST"><button type="input" name="m2">Interfaces externas al elemento software</button>
																	<form method="POST"><button type="input" name="m3">Requerimientos de calificación</button>
																	<form method="POST"><button type="input" name="m4">Especificaciones de seguridad física</button>
																	<form method="POST"><button type="input" name="m5">Especificaciones de seguridad de acceso</button>
																	<form method="POST"><button type="input" name="m6">Especificaciones relacionadas con ingeniería de factores humanos</button>
																	<form method="POST"><button type="input" name="m7">Definición de datos y Requerimientos de las bases de datos</button>
																	<form method="POST"><button type="input" name="m8">Requerimientos de instalación y aceptación del producto</button>
																	<form method="POST"><button type="input" name="">Documentación de usuario</button>
																	<form method="POST"><button type="input" name="m9">Requerimientos de operación y ejecución por parte del usuario</button>
																	<form method="POST"><button type="input" name="m10">Requerimientos de mantenimiento por parte del usuario</button>
																	<form method="POST"><button type="input" name="m11">Criterios de evaluación de los requerimientos software</button>
																	<form method="POST"><button type="input" name="m12">Documento de resultados de las evaluaciones</button>
																</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Diseño de la arquitectura del software
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="n1">Documento de la arquitectura del elemento software</button>
																	<form method="POST"><button type="input" name="n2">Documento de la arquitectura del elemento software</button>
																	<form method="POST"><button type="input" name="n3">Documento del diseño a alto nivel de las interfaces entre los componentes software del elemento software</button>
																	<form method="POST"><button type="input" name="n4">Documento del diseño a alto nivel de la base de datos</button>
																	<form method="POST"><button type="input" name="n5">Documento de versiones preliminares de la documentación de usuario</button>
																	<form method="POST"><button type="input" name="n6">Documento de los requerimientos preliminares de pruebas</button>
																	<form method="POST"><button type="input" name="n7">Criterios de evaluación de la arquitectura y sus diseños de interfaz y base de datos</button>
																	<form method="POST"><button type="input" name="n8">Documento de resultados de las evaluaciones</button>
																</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Diseño detallado del software
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="o1">Documento del diseño detallado de cada componente software del elemento software</button>
																	<form method="POST"><button type="input" name="o2">Documento del diseño detallado de las interfaces externas al elemento software, entre los componentes y las unidades software</button>
																	<form method="POST"><button type="input" name="o3">Documento del diseño detallado de la base de datos</button>
																	<form method="POST"><button type="input" name="o4">Documento de los requerimientos de prueba</button>
																	<form method="POST"><button type="input" name="o5">Criterios de evaluación del diseño detallado del software y los requerimientos de prueba</button>
																	<form method="POST"><button type="input" name="o6">Documento de resultados de las evaluaciones</button>
																</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Codificación y pruebas del software
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="p1">Documento de cada unidad de software y base de datos</button>
																	<form method="POST"><button type="input" name="p2">Documento de procedimientos de prueba</button>
																	<form method="POST"><button type="input" name="p3">Documento de resultados de las pruebas</button>
																	<form method="POST"><button type="input" name="p4">Criterios de evaluación del código software y los resultados de las pruebas</button>
																	<form method="POST"><button type="input" name="p5">Documento de resultados de las evaluaciones</button>
																</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Integración del software
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="q1">Documento de plan de integración</button>
																	<form method="POST"><button type="input" name="q2">Documento de los resultados de la integración y de las pruebas</button>
																	<form method="POST"><button type="input" name="q3">Documentación de conjunto de pruebas para cada requerimiento de calificación del elemento software</button>
																	<form method="POST"><button type="input" name="q4">Documentación de casos de prueba para cada requerimiento de calificación del elemento software</button>
																	<form method="POST"><button type="input" name="q5">Documentación de procedimientos de prueba para cada requerimiento de calificación del elemento software</button>
																	<form method="POST"><button type="input" name="q6">Criterios de evaluación del plan de integración, diseño, código, pruebas, resultados de pruebas y documentación de usuario</button>
																	<form method="POST"><button type="input" name="q7">Documento de resultados de las evaluaciones</button>
																</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Pruebas de calificación del software
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="r1">Documento de resultados de las pruebas de calificación</button>
																	<form method="POST"><button type="input" name="r2">Criterios de evaluación del diseño, código, pruebas, resultados de pruebas y documentación de usuario</button>
																	<form method="POST"><button type="input" name="r3">Documento de resultados de las evaluaciones</button>
																	<form method="POST"><button type="input" name="r4">Soporte para las auditorías</button>
																	<form method="POST"><button type="input" name="r5">Documento de resultados de las auditorías</button>
																</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Integracion del sistema
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="s1">Documento de resultados de la integración y pruebas</button>
																	<form method="POST"><button type="input" name="s2">Documentación de conjunto de pruebas para cada requerimiento de calificación del sistema</button>
																	<form method="POST"><button type="input" name="s3">Documentación de casos de prueba para cada requerimiento de calificación del sistema</button>
																	<form method="POST"><button type="input" name="s4">Documentación de procedimientos de prueba para cada requerimiento de calificación del sistema</button>
																	<form method="POST"><button type="input" name="s5">Criterios de evaluación del sistema integrado</button>
																	<form method="POST"><button type="input" name="s6">Documento de resultados de las evaluaciones</button>
																</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Pruebas de calificación del sistema
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="t1">Documento de resultados de las pruebas de calificación</button>
																	<form method="POST"><button type="input" name="t2">Criterios de evaluación del sistema</button>
																	<form method="POST"><button type="input" name="t3">Documento de resultados de las evaluaciones</button>
																	<form method="POST"><button type="input" name="t4">Soporte para las auditorías</button>
																	<form method="POST"><button type="input" name="t5">Documento de resultados de las auditorías</button>
																</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Instalación del software
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="u1">Documento del plan de instalación del software</button>
																	<form method="POST"><button type="input" name="u2">Documento de las incidencias y resultados de la instalación</button>
																</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Apoyo a la aceptación del software
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="v1">Documento de resultados de las pruebas y revisiones de aceptación</button>
																</div>
															</div>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</li>
									<!-- Termina Nivel 2-->
									<!-- Empieza Nivel 2-->
									<li>
										<div class="collapsible-header grey lighten-2">
											<i class="material-icons">filter_drama</i>Operación
										</div>
										<div class="collapsible-body">
											<div class="row">
												<div class="col s12 m12">
													<ul class="collapsible" data-collapsible="accordion">
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Implementación del proceso
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="w1">Documento del plan de operación del sistema</button>
																	<form method="POST"><button type="input" name="w2">Establecer procedimiento de gestión de problemas</button>
																	<form method="POST"><button type="input" name="w3">Proceso de solución de problemas</button>
																	<form method="POST"><button type="input" name="w4">Establecer procedimientos de prueba del software en un entorno de operación</button>
																</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Pruebas de
																operación
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="x1">Documento de resultados de las pruebas de operación</button>
																</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Operación del sistema
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Soporte al usuario
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="x2">Supervisión de las peticiones de usuario</button>
																	<form method="POST"><button type="input" name="x3">Tramitación de las peticiones de usuario en el proceso de mantenimiento</button>
																	<form method="POST"><button type="input" name="x4">Seguimiento del desarrollo de la petición de usuario</button>
																</div>
															</div>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</li>
									<!-- Termina Nivel 2-->
									<!-- Empieza Nivel 2-->
									<li>
										<div class="collapsible-header grey lighten-2">
											<i class="material-icons">filter_drama</i>Mantenimiento
										</div>
										<div class="collapsible-body">
											<div class="row">
												<div class="col s12 m12">
													<ul class="collapsible" data-collapsible="accordion">
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Implementación
																del proceso
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="y1">Documento del plan de mantenimiento</button>
																	<form method="POST"><button type="input" name="y2">Establecer procedimiento de gestión de problemas y peticiones de usuario</button>
																	<form method="POST"><button type="input" name="y3">Proceso de solución de problemas</button>
																	<form method="POST"><button type="input" name="y4">Gestión de la configuración</button>
																</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Análisis de
																problemas y modificaciones
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="y5">Documento del problema o petición de modificación</button>
																	<form method="POST"><button type="input" name="y6">Documento de resultados del análisis</button>
																	<form method="POST"><button type="input" name="y7">Documento de alternativas de implementación</button>
																</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Implementación
																de las modificaciones
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="y8">Documento del análisis de modificación requerida</button>
																	<form method="POST"><button type="input" name="y9">Documento de criterios de prueba y evaluación de las partes modificadas y no modificadas del sistema</button>
																	<form method="POST"><button type="input" name="y10">Documento de resultados de las pruebas</button>
																
															</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Revisión/aceptación
																del mantenimiento.
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="z1">Revisiones de la integridad del sistema modificado</button>
																
															</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Migración
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="z2">Documento del plan de migración</button>
																	<form method="POST"><button type="input" name="z3">Notificaciones del plan de migración</button>
																	<form method="POST"><button type="input" name="z4">Revisión post-operación del software</button>
																</div>
															</div>
														</li>
														<li>
															<div class="collapsible-header grey lighten-3">
																<i class="material-icons">filter_drama</i>Retirada del
																software
															</div>
															<div class="collapsible-body">
																<div class="collection">
																	<form method="POST"><button type="input" name="z5">Documento del plan de retirada</button>
																	<form method="POST"><button type="input" name="z6">Notificaciones del plan de retirada</button>
																</div>
															</div>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</li>
									<!-- Termina Nivel 2-->
								</ul>
							</div>
						</div>
					</div>
				</li>
				<!-- Termina Nivel 1-->
				<!-- Empieza Nivel 1-->
				<li>
					<div class="collapsible-header grey lighten-1">

						<a href="#" onclick="cargar('#reemplazar', 'grafico2.php')"><i class="material-icons">filter_drama</i> Ver Reporte General</a>

						
					</div>
					<div class="collapsible-body">
	
					</div>
				</li>
				<!-- Termina Nivel 1-->

				<!-- Empieza Roles-->
				<li>
					<div class="collapsible-header grey lighten-1">

						<a href="roles.php">
							<i class="material-icons">filter_drama</i> Roles
						</a>


					</div>
					<div class="collapsible-body">

					</div>
				</li>
				<!-- Termina Roles-->
				
			</ul>

		</div>

		<div class="col s9" id="reemplazar">
			<div class="container">
				<div class="section">
					<div class="row">
						<form class="col s6">
							<div class="row">
								<h5 class="header col s12 light">Tabla de Caracteristicas y Metricas</h5>
																<div >
<h6>Cobertura de la actividad</h6>

<form action="/action_page.php">
  <select class="btn waves-effect waves-light grey darken-1">
  <option value="1" style="background-color: green">Completo</option>
  <option value="2" style="background-color: yellow">Parcialmente Completo</option>
 <option value="3" style="background-color: red">Incompleto</option>

</select>
  <br><br>
</form>								
</div>
		
	<div>
<h6>Complejidad de la actividad</h6>

<form action="/action_page.php">
  <select class="btn waves-effect waves-light grey darken-1">
  <option value="1" style="background-color: green">Completo</option>
  <option value="2" style="background-color: yellow">Parcialmente Completo</option>
 <option value="3" style="background-color: red">Incompleto</option>

</select>
  <br><br>
<br><br>
</form>								
</div>

<div>
<form method="get" action="">

<button type="submit">

<b>Agregar Evidencia</b><br />

</button>

</form>
</div>

<div>
<table>
  <tr>
    <th>Lista de Evidencias</th>
  </tr>
  <tr>
    <td>lalalal.pdf</td>
  </tr>
</table>
</div>


										</div>
						</form>
						
					</div>
				</div>
				<br> <br>
			</div>
		</div>
		
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script>
function cargar(div, desde)
{
     $(div).load(desde);
}
</script>

	</div>

	<!--  Scripts-->
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="js/materialize.js"></script>
	<script src="js/init.js"></script>

</body>
</html>

