<?php
	session_start();
	if (isset($_SESSION["usuario"])){
		if ($_SESSION["meth"] == "N") {
			
		}
	}else{
		header("Location: index.php");
	}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<meta http-equiv="content-type"
	content="application/xhtml+xml; charset=UTF-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1.0" />
<title>Starter Template - Materialize</title>

<!-- CSS  -->

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="css/materialize.css" type="text/css" rel="stylesheet"
	media="screen,projection" />
<link href="css/style.css" type="text/css" rel="stylesheet"
	media="screen,projection" />
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
</head>
<body>
	<nav class="grey lighten-1 center" role="navigation">
		<div class="nav-wrapper container" id="todo">
			<a id="logo-container" href="#" class="brand-logo">ICACIT Quest</a>
			<ul class="right hide-on-med-and-down">
				<li><a>Cerrar Sesión</a></li>
			</ul>
		</div>
	</nav>


	<!-- Page Layout here -->
	<div class="row">
		<div class="col s3">

			<ul class="collapsible" data-collapsible="accordion">


				<!-- Empieza Nivel 1-->
				<li>
					<div class="collapsible-header grey lighten-1">
						<i class="material-icons">filter_drama</i>Proceso Principal
					</div>
					<div class="collapsible-body">
						<div class="row">
							<div class="col s12 m12">
								<ul class="collapsible" data-collapsible="accordion">
									<li>
										<div class="collapsible-header grey lighten-2">
									 <i class="material-icons">filter_drama</i>Criterio 1: Estudiantes: el desarrollo profesional y la inserción laboral
										</div>
															<div class="collapsible-body">
																<div class="collection">
<form method="POST">
<button type="input" name="a1">Evaluación del desempeño del estudiante</button>
<button type="input" name="b1">Asesoramiento a los estudiantes respecto al plan de estudios  y la profesión</button>
<button type="input" name="c1">Admisión de estudiantes nuevos y  de traslado</button>
<button type="input" name="d1">Otorgar créditos académicos apropiados por cursos externos</button>
<button type="input" name="b1">Desarrollo de actividades extracurriculares</button>
<button type="input" name="b1">Verificación que los estudiantes graduados cumplan con requisitos para la graduación</button>
</form>
																</div>
															</div>
														
									</li>





									<!-- Empieza Nivel 2-->
									<li>
										<div class="collapsible-header grey lighten-2">
											<i class="material-icons">filter_drama</i>
											Criterio 2: Objetivos educativos del programa: vaya acorde con la misión
										</div>
				
														<div class="collapsible-body">
																<div class="collection">
<button>Objetivos educacionales publicados</button>
<button>Plan de revisión periódica de objetivos</button>
														</div>
															</div>
													
									</li>
									<!-- Termina Nivel 2-->



									<!-- Empieza Nivel 2-->
									<li>
										<div class="collapsible-header grey lighten-2">
											<i class="material-icons">filter_drama</i>Criterio 3. Resultados del programa: al graduarse tenga los conocimientos necesarios. 
										</div>
				
															<div class="collapsible-body">
																<div class="collection">
<button>Conocimientos de ingenieri­a</button>
	<button>Experimentacion</button>
<button>Diseño y desarrollo de soluciones</button>
<button>Trabajo individual y en equipo</button>
<button>Analisis de problemas</button>
	<button>Etica</button>
<button>Comunicación</button>
<button>Medio ambiente y sostenibilidad</button>
<button>Aprendizaje permanente</button>
<button>El ingeniero y la sociedad</button>
<button>Uso de herramientas modernas</button>
<button>Gestión de proyectos</button>

																</div>
															</div>
								
									</li>
									<!-- Termina Nivel 2-->




									<!-- Empieza Nivel 2-->
									<li>
										<div class="collapsible-header grey lighten-2">
											<i class="material-icons">filter_drama</i>Criterio 4. Mejora continua: métodos apropiados en la medición
										</div>
			
															<div class="collapsible-body">
																<div class="collection">
										<button>Procesos documentados y apropiados en el assesment</button>
<button>Evaluación del grado en que los resultados del estudiante están siendo logrados</button>

																</div>
															</div>

									</li>
									<!-- Termina Nivel 2-->



				
							








																		<!-- Empieza Nivel 2-->
									<li>
										<div class="collapsible-header grey lighten-2">
											<i class="material-icons">filter_drama</i>Criterio 5. Plan de estudios: matemática y ciencia básicas, típicos de ingeniería y luego general
										</div>

															<div class="collapsible-body">
																<div class="collection">
<button>Un año de una combinación de matemáticas de nivel universitario  y ciencias básicas algunas de ellas con parte experimental apropiadas para la disciplina. </button>
<button>Un año y medio de tópicos de ingenierí­a, que comprendan ciencias de la ingenierí­a  y diseño en ingeniería apropiados para el campo de estudios del estudiante</button>
<button>Un componente de educación general que complemente el contenido técnico del plan de estudios, y que sea consistente con los objetivos del programa y la institución</button>

																</div>
															</div>
									 </li>


									<!-- Termina Nivel 2-->






																		<!-- Empieza Nivel 2-->
									<li>
										<div class="collapsible-header grey lighten-2">
											<i class="material-icons">filter_drama</i>Criterio 6. Cuerpo de Profesores: desarrollo profesional de sus profesores. 
										</div>

															<div class="collapsible-body">
																<div class="collection">
<form method="POST">
<button type="input" name="a6">Número suficiente de profesores  para la  interacción entre estudiantes y profesores</button>
<button type="input" name="b6">Número suficiente de profesores para consejería y orientación a los estudiantes</button>
<button type="input" name="c6">Número suficiente de profesores para actividades de servicio universitario</button>
<button type="input" name="d6">Número suficiente de profesores para desarrollo profesional</button>
<button type="input" name="e6">Número suficiente de profesores para la  interacción con representantes de la industria y la profesión, así como con los empleadores de los estudiantes</button>
</form>


																</div>
															</div>
									 </li>


									<!-- Termina Nivel 2-->










																		<!-- Empieza Nivel 2-->
									<li>
										<div class="collapsible-header grey lighten-2">
											<i class="material-icons">filter_drama</i>Criterio 7. Instalaciones: adecuados para el desarrollo del estudiante
										</div>

															<div class="collapsible-body">
																<div class="collection">
<button>Cuenta con oficinas, salas de clase, laboratorios y equipos  adecuados y  un clima propicio para el aprendizaje. </button>
<button>Cuenta con recursos informáticos y laboratorios disponibles, y sistemáticamente mantenidos y actualizados</button>
<button>Guía en el uso de recursos informáticos </button>
<button>Servicios de biblioteca e infraestructura informática para estudiantes y profesores</button>


																</div>
															</div>
									 </li>


									<!-- Termina Nivel 2-->







																		<!-- Empieza Nivel 2-->
									<li>
										<div class="collapsible-header grey lighten-2">
											<i class="material-icons">filter_drama</i>Criterio 8. Apoyo (institucional): autoridades de la institución adecuadas para el desarrollo.
										</div>

															<div class="collapsible-body">
																<div class="collection">
<button>Apoyo y liderazgo institucional adecuado para asegurar la calidad</button>
<button>Recursos, servicios institucionales, financieros y personal adecuado  deben ser adecuados para satisfacer sus necesidades</button>
<button>Recursos suficientes para atraer, retener y proveer el desarrollo profesional</button>


																</div>
															</div>
									 </li>


									<!-- Termina Nivel 2-->






																		<!-- Empieza Nivel 2-->
									<li>
										<div class="collapsible-header grey lighten-2">
											<i class="material-icons">filter_drama</i>Criterio 9. Investigación e InnovaciÃ³n: regular y asegurar la calidad de las investigaciones
										</div>

															<div class="collapsible-body">
																<div class="collection">
<button>El programa regula y asegura la calidad de las investigaciones de sus profesores</button>
<button>Las investigaciones de los profesores  consistentes con la disciplina del programa y las políticas de investigación de la institución</button>
<button>El programa asegura la rigurosidad, pertinencia y calidad de los trabajos de investigación realizados por sus estudiantes para la obtención del grado académico</button>
<button>El programa promueve la publicación de los trabajos de investigación de sus profesores y su respectiva socialización dentro de la institución</button>


																</div>
															</div>
									 </li>


									<!-- Termina Nivel 2-->



								</ul>
							</div>
						</div>
					</div>
				</li>
				<!-- Termina Nivel 1-->
<!-- Empieza Nivel 1-->
				<li>
					<div class="collapsible-header grey lighten-1">

						<a href="#" onclick="cargar('#reemplazar', 'grafica.php')"><i class="material-icons">filter_drama</i> Ver Reporte General</a>

						
					</div>
					<div class="collapsible-body">
	
					</div>
				</li>
				<!-- Termina Nivel 1-->
				<!-- Empieza Roles-->
				<li>
					<div class="collapsible-header grey lighten-1">

						<a href="roles.php">
							<i class="material-icons">filter_drama</i> Roles
						</a>


					</div>
					<div class="collapsible-body">

					</div>
				</li>
				<!-- Termina Roles-->
				
		
			</ul>

		</div>

		<div class="col s9" id="reemplazar">
			<div class="container">
				<div class="section">
					<div class="row">
						<form class="col s6">
							<div class="row">
								<h5 class="header col s12 light">Tabla de Caracteristicas y Metricas</h5>
								<div >
									<h6>Cobertura de la actividad</h6>

									<form action="/action_page.php">
										<select class="btn waves-effect waves-light grey darken-1">
											<option value="1" style="background-color: green">Completo</option>
											<option value="2" style="background-color: yellow">Parcialmente Completo</option>
											<option value="3" style="background-color: red">Incompleto</option>

										</select>
										<br>
											<br>
</form>
								</div>

								<div>
									<h6>Complejidad de la actividad</h6>

									<form action="/action_page.php">
										<select class="btn waves-effect waves-light grey darken-1">
											<option value="1" style="background-color: green">Completo</option>
											<option value="2" style="background-color: yellow">Parcialmente Completo</option>
											<option value="3" style="background-color: red">Incompleto</option>

										</select>
													<br>
														<br>

									</form>
								</div>

								<div>
									<form method="get" action="">

										<button type="submit">

											<b>Agregar Evidencia</b>
											<br />

										</button>

									</form>
								</div>

								<div>
									<table>
										<tr>
											<th>Lista de Evidencias</th>
										</tr>
										<tr>
											<td>lalalal.pdf</td>
										</tr>
									</table>
								</div>


							</div>
						</form>

					</div>
				</div>
				<br>
					<br>
			</div>
		</div>
		
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script>
function cargar(div, desde)
{
     $(div).load(desde);
}
</script>

	</div>

	<!--  Scripts-->
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="js/materialize.js"></script>
	<script src="js/init.js"></script>
	
	


</body>
</html>
