<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<meta http-equiv="content-type"
	content="application/xhtml+xml; charset=UTF-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1.0" />

<!-- CSS  -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="css/materialize.css" type="text/css" rel="stylesheet"
	media="screen,projection" />
<link href="css/style.css" type="text/css" rel="stylesheet"
	media="screen,projection" />
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
  	<script>
  		$(document).ready(function(){
  			$("#open").click(function(){
  				$("#todo").load("http://localhost:8069/web#page=0&limit=80&view_type=list&model=purchase.order&menu_id=309&action=348");
  			});
  		});

	</script>
</head>
<body>

	<!-- ChartJS -->
	<script src="chart.js/Chart.js"></script>



<div class="container-fluid" id="solo">
	<div class="row">
		<div class="col-md-3">
			
		</div>
		<div class="col-md-9">
			<div class="box box-success">
            <div class="box-header with-border">
            	<?php
            		//set timezone
					date_default_timezone_set('America/El_Salvador');
					$year = date('Y');
            	?>
              <h3 class="box-title">Reporte General</h3>

            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="barChart" width="400px" height="400px"  ></canvas>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
		</div>
	</div>
</div>
<?php include('data.php'); ?>
<script>
  $(function () {
    var barChartData = {
      labels  : ['Proceso 1', 'Proceso 2', 'Proceso 3'],
      datasets: [
        {
          
        },
        {
          label               : 'Ventas',
          fillColor           : 'rgba(60,141,188,0.9)',
          strokeColor         : 'rgba(60,141,188,0.8)',
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : [ "<?php echo $tjan; ?>",
                                  "<?php echo $tfeb; ?>",
                                  "<?php echo $tmar; ?>",
                                  "<?php echo $tapr; ?>",
                                  "<?php echo $tmay; ?>",
                                  "<?php echo $tjun; ?>",
                                  "<?php echo $tjul; ?>",
                                  "<?php echo $taug; ?>",
                                  "<?php echo $tsep; ?>",
                                  "<?php echo $toct; ?>",
                                  "<?php echo $tnov; ?>",
                                  "<?php echo $tdec; ?>" 
                                ]
        }
      ]
    }
    var barChartCanvas                   = $('#barChart').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = barChartData
    barChartData.datasets[1].fillColor   = '#00a65a'
    barChartData.datasets[1].strokeColor = '#00a65a'
    barChartData.datasets[1].pointColor  = '#00a65a'
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 5,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions)

  })
</script>

</div>
*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/**/*/*/*/*/*/*/**/*/*/*/*/*/*/*/*/*
<ul>
      <li>Criterios
        <ul>
          <li><a href="#" onclick="cargar('#solo', 'proceso1.php')">Proceso Principal</a>: Estudiantes: el desarrollo profesional y la inserci�n laboral.</li>
	   <li><a href="#"  onclick="cargar('#solo', 'proceso2.php')">Proceso de Apoyo</a>: Objetivos educativos del programa: vaya acorde con la misi�n.</li>
	 <li><a href="#"  onclick="cargar('#solo', 'proceso3.php')">Proceso de Organizativos</a>: Resultados del programa: al graduarse tenga los conocimientos necesarios</li>

         
        </ul>
</ul>


</body>
</html>

