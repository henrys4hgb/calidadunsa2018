<?php
	session_start();
	if (isset($_SESSION["usuario"])){
		if(isset($_POST['btn'])){
			$m = $_POST['meto'];
			if($m == "ICACIT"){
				$_SESSION["meth"] = "I";
			}
			if($m == "NTP"){
				$_SESSION["meth"] = "N";
			}
			if(isset($_POST['emp_name'])){
				$_SESSION["emp_name"] = $_POST['emp_name']; 
			}else{
				echo "No se puso nombre";
			}
		}
		if(isset($_POST['btnSubmit'])){
			if(isset($_FILES['myfile'])){
				$_SESSION["logo"] = file_get_contents($_FILES['myfile']['tmp_name']);
			}
		}
		
		if(isset($_SESSION["emp_name"])){
			if (isset($_SESSION["logo"])) {
				header("Location: prueba.php");
			}
		}

	}else{
		header("Location: index.php");
	}

	if(isset($_POST['close'])){ 
		session_destroy(); 
	}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<meta http-equiv="content-type"
	content="application/xhtml+xml; charset=UTF-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1.0" />
<title>Starter Template - Materialize</title>

<!-- CSS  -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="css/materialize.css" type="text/css" rel="stylesheet"
	media="screen,projection" />
<link href="css/style.css" type="text/css" rel="stylesheet"
	media="screen,projection" />
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
</head>
<body>
	<nav class="grey lighten-1 center" role="navigation">
		<div class="nav-wrapper container" id="todo">
			<a id="logo-container" href="#" class="brand-logo">NtpQuest</a>
			<ul class="right hide-on-med-and-down">
				<li>
					<form method="post">
						<button name = "close" type="submit">Cerrar Sesión</button>
					</form>
				</li>
			</ul>
		</div>
	</nav>


	<!-- Page Layout here -->
	<div class="row">
		<div class="col s3">

			<ul class="collapsible" data-collapsible="accordion">
																								</ul>
							</div>
						</div>
					</div>
				</li>
				<!-- Termina Nivel 1-->
			</ul>

		</div>

		<div class="col s9">
			<div class="container">
				<div class="section">
					<div class="row">
						<form class="col s6" method="post">
							<div class="row">
								<h5 class="header col s12 light">Modificar</h5>
								<div class="input-field col s12">
									<input id="first_name" type="text" class="validate" name="emp_name">
										<label
										for="first_name">Nombre de empresa</label>
									</input>
								</div>
								<div class="input-field col s12">
									<select class="btn waves-effect waves-light grey darken-1" name="meto">
										<option value="NTP">NTP</option>
										<option value="ICACIT">ICACIT</option>
									</select>
								</div>
								<div class="input-field col s12">
									<button class="btn waves-effect waves-light grey darken-1"
										type="submit" name="btn">Modificar</button>
								</div>
							</div>
						</form>
						<div class="row">
							<form class="col s3" enctype="multipart/form-data" method="post">
								<input type="file" name="myfile" /> 
								<br>
								<br> 
								<input type="submit" name="btnSubmit" value="Subir Logo" /> <br />
								<div id="result">
									<?php
										if(isset($_SESSION["logo"])){
											echo '<img class="materialboxed" style="width: 100%" src="data:image/jpeg;base64,'.base64_encode($_SESSION["logo"]).'"/>';
										}
									?>
								</div> 
							</form>

						</div>
					</div>
				</div>
				<br> <br>
			</div>
		</div>

	</div>

	<!--  Scripts-->
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="js/materialize.js"></script>
	<script src="js/init.js"></script>

</body>
</html>

