<?php
session_start();
if (isset($_SESSION["usuario"])){

	if (isset($_SESSION["definido"])){
		$dd = $_SESSION["definido"];
	}else{
		header("Location: principal.php");
	}
}

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<meta http-equiv="content-type"
	content="application/xhtml+xml; charset=UTF-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1.0" />
<title>Starter Template - Materialize</title>

<!-- CSS  -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="css/materialize.css" type="text/css" rel="stylesheet"
	media="screen,projection" />
<link href="css/style.css" type="text/css" rel="stylesheet"
	media="screen,projection" />
</head>
<body>
	<nav class="grey lighten-1 center" role="navigation">
		<div class="nav-wrapper container">
			<a id="logo-container" href="#" class="brand-logo">Quest</a>
		</div>
	</nav>
	<div class="section no-pad-bot" id="index-banner">
		<div class="container">
			<br> <br>
			<h1 class="header center orange-text">Quest</h1>
			<div class="row center">
				<h5 class="header col s12 light">Sistema de organizacion de
					documentos para la acreditacion ICACIT</h5>
			</div>
		</div>
	</div>


	<div class="container">
		<div class="section">

			<div class="row">
				<form class="col s12" action="connection.php" method="POST">
					<div class="row">
						<div class="input-field col s4 offset-s4">
							<input id="user" type="text" name="user" class="validate"> <label
								for="user">Usuario</label>
						</div>
						<div class="input-field col s4 offset-s4">
							<input id="password" type="password" name="password" class="validate"> <label
								for="password">Contraseña</label>
						</div>
						<?php
							if (isset($_SESSION["errorUsr"])){
								$ptr = $_SESSION["errorUsr"];
								echo $ptr;
								unset($_SESSION["errorUsr"]);
							}
						?>
						<div class="input-field col s4 offset-s4">
							<a href="registro.php">Registrarse</a>
						</div>
						<div class="input-field col s4 offset-s4">
							<button class="btn waves-effect waves-light grey darken-1" type="submit"
								name="action">Ingresar</button>
						</div>
					</div>
				</form>
			</div>

		</div>
		<br> <br>
	</div>
	<!--  Scripts-->
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="js/materialize.js"></script>
	<script src="js/init.js"></script>

</body>
</html>
